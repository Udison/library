package ru.udison.task.library.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.udison.task.library.entity.Genre;

import java.util.List;

public interface GenresDao extends JpaRepository<Genre, Long> {
    boolean existsByGenre(String genre);
}
