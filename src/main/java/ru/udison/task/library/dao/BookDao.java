package ru.udison.task.library.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.udison.task.library.entity.Author;
import ru.udison.task.library.entity.Book;
import ru.udison.task.library.entity.Genre;

import java.util.List;

public interface BookDao extends JpaRepository<Book, Long> {
    List<Book> findAllByAuthor(Author author);
    List<Book> findAllByGenre(Genre genre);
}
