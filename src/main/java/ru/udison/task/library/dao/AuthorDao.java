package ru.udison.task.library.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.udison.task.library.entity.Author;

import java.util.List;
import java.util.Optional;

public interface AuthorDao extends JpaRepository<Author, Long> {
    Author findByLastNameAndFirstNameAndPatronymic(String lastName, String firstName, String patronymic);
    boolean existsAuthorByLastNameAndFirstNameAndPatronymic(String lastName, String firstName, String patronymic);
}
