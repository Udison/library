package ru.udison.task.library.ui.spaces.addspaces;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.udison.task.library.dao.AuthorDao;
import ru.udison.task.library.dao.BookDao;
import ru.udison.task.library.dao.GenresDao;
import ru.udison.task.library.entity.Author;
import ru.udison.task.library.entity.Book;
import ru.udison.task.library.entity.Genre;

import java.util.Optional;
import java.util.stream.Collectors;

@Data
@Component
@RequiredArgsConstructor
public class BooksAddSpace extends Window {
    private final AuthorDao authorDao;
    private final BookDao bookDao;
    private final GenresDao genresDao;
    private Binder<Book> bookBinder;
    private Binder<Author> authorBinder;
    private Binder<Genre> genreBinder;


    public Window drawBookAddWindow() {
        Window addWindow = new Window("Add new book");
        AddSpaceConfig.configureAddWindow(addWindow, "700px", "900px");
        Book book = new Book();
        book.setAuthor(new Author());
        book.setGenre(new Genre());
        initBinders(book);

        VerticalLayout contentLayout = new VerticalLayout();
        contentLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);

        contentLayout.addComponents(createTitleSection(),
                createAuthorSection(),
                createGenreSection(),
                createPublisherSection(),
                createCityAndYearSection(),
                createSaveAndCancelButtons(book, addWindow));

        addWindow.setContent(contentLayout);

        UI.getCurrent().addWindow(addWindow);

        return addWindow;
    }

    private HorizontalLayout createSaveAndCancelButtons(Book book, Window editWindow) {

        HorizontalLayout toReturn = new HorizontalLayout();
        Button save = new Button("Save", clickEvent -> {
            try {
                Author author = book.getAuthor();
                Genre genre = book.getGenre();
                authorBinder.writeBean(author);
                genreBinder.writeBean(genre);
                bookBinder.writeBean(book);
                if(authorDao.existsAuthorByLastNameAndFirstNameAndPatronymic(author.getLastName(), author.getFirstName(), author.getPatronymic())) {
                    Notification.show("Author is exist yet", Notification.Type.WARNING_MESSAGE);
                    return;
                }
                if (genresDao.existsByGenre(genre.getGenre())) {
                    Notification.show("Genre is exist yet", Notification.Type.WARNING_MESSAGE);
                    return;
                }

                bookDao.save(book);
                Notification.show("Book was saved");
                editWindow.close();
            } catch (ValidationException e) {
                Notification.show("Book could not be saved,\n" +
                        "please check error messages for each field.", Notification.Type.ERROR_MESSAGE);
            }
        });


        save.setStyleName(ValoTheme.BUTTON_PRIMARY);

        Button cancel = new Button("Cancel", clickEvent -> editWindow.close());

        toReturn.addComponents(save, cancel);

        return toReturn;
    }

    private HorizontalLayout createCityAndYearSection() {
        HorizontalLayout toReturn = new HorizontalLayout();
        TextField cityField = new TextField("City:");
        cityField.setIcon(VaadinIcons.FACTORY);
        cityField.setStyleName(ValoTheme.TEXTFIELD_ALIGN_CENTER);
        cityField.setMaxLength(50);
        bookBinder.forField(cityField)
                .withValidator(s -> (s.matches("[a-zа-яёA-ZА-ЯЁ]+") || s.matches("")), "\"You can use letters а-Я or a-Z\"")
                .bind(Book::getCity, Book::setCity);

        TextField yearField = new TextField("Publishing year");
        yearField.setIcon(VaadinIcons.CALENDAR);
        yearField.setStyleName(ValoTheme.TEXTFIELD_ALIGN_CENTER);
        yearField.setMaxLength(4);
        bookBinder.forField(yearField)
                .withValidator(s -> (s.matches("\\d+") || s.matches("")), "Wrong data format\n" + "You can use numbers 0-9")
                .bind(Book::getPublishingYear, Book::setPublishingYear);

        toReturn.addComponentsAndExpand(cityField, yearField);

        return toReturn;

    }

    private HorizontalLayout createPublisherSection() {
        HorizontalLayout toReturn = new HorizontalLayout();
        NativeSelect<String> publisherSelect = new NativeSelect<>();
        publisherSelect.setCaption("Select a publisher");
        publisherSelect.setRequiredIndicatorVisible(true);
        publisherSelect.setItems("Москва", "Питер", "O'Reilly");
//        publisherSelect.setValue("");
        toReturn.addComponentsAndExpand(publisherSelect);
        bookBinder.forField(publisherSelect)
                .withValidator(s -> !s.isEmpty(), "Select a publisher")
                .bind(Book::getPublisher, Book::setPublisher);

        return toReturn;

    }

    private HorizontalLayout createGenreSection() {
        HorizontalLayout toReturn = new HorizontalLayout();

        TextField genreField = new TextField("Genre:");
        genreField.setIcon(VaadinIcons.PENCIL);
        genreField.setStyleName(ValoTheme.TEXTFIELD_ALIGN_CENTER);
        genreField.setRequiredIndicatorVisible(true);
        genreField.setMaxLength(50);
        toReturn.addComponentsAndExpand(genreField);
        genreBinder.forField(genreField)
                .withValidator(s -> s.matches("[a-zа-яёA-ZА-ЯЁ]+"), "\"You can use letters а-Я or a-Z\"")
                .bind(Genre::getGenre, Genre::setGenre);

        return toReturn;
    }

    private HorizontalLayout createAuthorSection() {
        HorizontalLayout toReturn = new HorizontalLayout();
        VerticalLayout components = new VerticalLayout();
        Label authorMark = new Label("Author", ContentMode.TEXT);
        authorMark.setStyleName(ValoTheme.TEXTFIELD_ALIGN_CENTER);
        components.addComponentsAndExpand(authorMark);

        HorizontalLayout authorSection = new HorizontalLayout();

        TextField lastName = new TextField("Last name:");
        lastName.setIcon(VaadinIcons.USER);
        lastName.setStyleName(ValoTheme.TEXTFIELD_ALIGN_CENTER);
        lastName.setRequiredIndicatorVisible(true);
        lastName.setMaxLength(30);
        authorBinder.forField(lastName)
                .withValidator(s -> s.matches("[a-zа-яёA-ZА-ЯЁ]+"), "You can use letters а-Я or a-Z")
                .bind(Author::getLastName, Author::setLastName);

        TextField firstName = new TextField("First name:");
        firstName.setIcon(VaadinIcons.USER);
        firstName.setStyleName(ValoTheme.TEXTFIELD_ALIGN_CENTER);
        firstName.setRequiredIndicatorVisible(true);
        firstName.setMaxLength(30);
        authorBinder.forField(firstName)
                .withValidator(s -> s.matches("[a-zа-яёA-ZА-ЯЁ]+"), "You can use letters а-Я or a-Z")
                .bind(Author::getFirstName, Author::setFirstName);

        TextField patronymic = new TextField("Patronymic:");
        patronymic.setIcon(VaadinIcons.USER);
        patronymic.setStyleName(ValoTheme.TEXTFIELD_ALIGN_CENTER);
        patronymic.setMaxLength(30);
        authorBinder.forField(patronymic)
                .withValidator(s -> (s.matches("[a-zа-яёA-ZА-ЯЁ]+") || s.matches("")), "You can use letters а-Я or a-Z")
                .bind(Author::getPatronymic, Author::setPatronymic);



        authorSection.addComponentsAndExpand(lastName, firstName, patronymic);
        components.addComponentsAndExpand(authorMark, authorSection);
        toReturn.addComponentsAndExpand(components);
        return toReturn;
    }

    private HorizontalLayout createTitleSection() {
        HorizontalLayout toReturn = new HorizontalLayout();
        VerticalLayout titleSection = new VerticalLayout();
        TextField title = new TextField("  Title:");
        title.setWidth("800px");
        title.setIcon(VaadinIcons.BOOK);
        title.setStyleName(ValoTheme.TEXTFIELD_ALIGN_CENTER);
        title.setStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);
        title.setRequiredIndicatorVisible(true);
        title.setMaxLength(80);
        bookBinder.forField(title)
                .withValidator(s -> s.matches("[a-zа-яёA-ZА-ЯЁ\\s]+"), "You can use letters а-Я or a-Z")
                .bind(Book::getTitle, Book::setTitle);
        Label counter = new Label();
        counter.setValue(title.getValue().length() +
                " of " + title.getMaxLength());

        // Display the current length interactively in the counter
        title.addValueChangeListener(event -> {
            int len = event.getValue().length();
            counter.setValue(len + " of " + title.getMaxLength());
        });
        title.setValueChangeMode(ValueChangeMode.EAGER);
        titleSection.setWidth("900px");
        titleSection.addComponents(title, counter);
        toReturn.addComponent(titleSection);

        return toReturn;
    }

    private void initBinders(Book book) {
        bookBinder = new Binder<>();
        bookBinder.readBean(book);
        authorBinder = new Binder<>();
        authorBinder.readBean(book.getAuthor());
        genreBinder = new Binder<>();
        genreBinder.readBean(book.getGenre());
    }

}
