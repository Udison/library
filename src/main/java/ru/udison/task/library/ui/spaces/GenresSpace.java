package ru.udison.task.library.ui.spaces;

import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.ui.*;
import com.vaadin.ui.renderers.ButtonRenderer;
import com.vaadin.ui.themes.ValoTheme;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.udison.task.library.dao.BookDao;
import ru.udison.task.library.dao.GenresDao;
import ru.udison.task.library.entity.Book;
import ru.udison.task.library.entity.Genre;
import ru.udison.task.library.ui.spaces.addspaces.GenresAddSpace;
import ru.udison.task.library.ui.spaces.editspaces.GenresEditSpace;

import javax.annotation.PostConstruct;
import java.util.List;

@Data
@Component
@RequiredArgsConstructor
public class GenresSpace extends VerticalLayout {
    private Grid<Genre> genreGrid;
    private final GenresDao genresDao;
    private final BookDao bookDao;
    private final GenresEditSpace genresEditSpace;
    private final GenresAddSpace addGenreWindow;

    @PostConstruct
    public void init() {
        createAddButton();
        createGenreGrid();
    }


    private void createAddButton() {
        Button addNewGenre = new Button("Add new Genre", event -> {
            Window addWindow = addGenreWindow.drawGenreAddWindow();
            addWindow.addCloseListener(e -> genreGrid.setItems(genresDao.findAll()));
        });
        addNewGenre.setStyleName(ValoTheme.BUTTON_PRIMARY);
        addComponent(addNewGenre);
    }

    private void createGenreGrid() {
        genreGrid = new Grid<>(Genre.class);
        ListDataProvider<Genre> provider = new ListDataProvider<>(genresDao.findAll());
        genreGrid.setDataProvider(provider);
        genreGrid.removeColumn("genreId");

        genreGrid.addColumn(genre -> "Edit", new ButtonRenderer<>(clickEvent -> {
            Window editWindow = genresEditSpace.drawGenreEditWindow(clickEvent.getItem());
            editWindow.addCloseListener(e -> genreGrid.setItems(genresDao.findAll()));
        })).setWidth(90.0);
        genreGrid.addColumn(author -> "Remove", new ButtonRenderer<>(clickEvent -> {
            List<Book> booksWithThisGenre = bookDao.findAllByGenre(clickEvent.getItem());
            if(booksWithThisGenre.isEmpty()) {
                genresDao.delete(clickEvent.getItem());
                genreGrid.setItems(genresDao.findAll());
            }
            else {
                Notification.show("Can't remove this genre. The library has books of this one.", Notification.Type.WARNING_MESSAGE);
            }
        })).setWidth(100.0);

        genreGrid.setWidth("100%");
        addComponentsAndExpand(genreGrid);
    }

    public void updateItems() {
        genreGrid.setItems(genresDao.findAll());
    }

}
