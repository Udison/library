package ru.udison.task.library.ui.spaces.addspaces;

import com.vaadin.ui.Window;

public class AddSpaceConfig {

    public static void configureAddWindow(Window addWindow, String height, String width) {
        addWindow.center();
        addWindow.setResizable(true);
        addWindow.setModal(true);
        addWindow.setClosable(true);
        addWindow.setResizable(true);
        addWindow.setDraggable(true);
        addWindow.setHeight(height);
        addWindow.setWidth(width);
    }
}
