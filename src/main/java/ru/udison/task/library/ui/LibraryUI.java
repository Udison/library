package ru.udison.task.library.ui;

import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import ru.udison.task.library.ui.spaces.WorkSpace;

@Data
@SpringUI
@RequiredArgsConstructor
public class LibraryUI extends UI {
    private VerticalLayout root;
    private final WorkSpace workSpace;


    @Override
    protected void init(VaadinRequest vaadinRequest) {
        setupLayout();
        addHeader();
        root.addComponentsAndExpand(workSpace);
    }

    private void setupLayout() {
        root = new VerticalLayout();
        root.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        setContent(root);
    }

    private void addHeader() {
        Label header = new Label("Library");
        header.addStyleName(ValoTheme.LABEL_H1);
        root.addComponent(header);
    }

}
