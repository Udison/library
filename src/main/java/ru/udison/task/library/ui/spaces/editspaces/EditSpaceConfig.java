package ru.udison.task.library.ui.spaces.editspaces;

import com.vaadin.ui.Window;

class EditSpaceConfig {

    static void configureEditWindow(Window editWindow, String height, String width) {
        editWindow.center();
        editWindow.setResizable(true);
        editWindow.setModal(true);
        editWindow.setClosable(true);
        editWindow.setResizable(true);
        editWindow.setDraggable(true);
        editWindow.setHeight(height);
        editWindow.setWidth(width);
    }
}
