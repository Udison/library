package ru.udison.task.library.ui.spaces;

import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Data
@Component
@RequiredArgsConstructor
public class WorkSpace extends VerticalLayout {

    private final BooksSpace booksSpace;
    private final AuthorsSpace authorsSpace;
    private final GenresSpace genresSpace;

    @PostConstruct
    private void createWorkSpace() {
        TabSheet tabSheet = new TabSheet();
        tabSheet.addListener(event -> {
            authorsSpace.updateItems();
            genresSpace.updateItems();
            booksSpace.updateItems();
        });
        VerticalLayout booksTab = new VerticalLayout();
        booksTab.addComponent(booksSpace);

        VerticalLayout authorTab = new VerticalLayout();
        authorTab.addComponent(authorsSpace);

        VerticalLayout genreTab = new VerticalLayout();
        genreTab.addComponent(genresSpace);

        tabSheet.addTab(booksTab, "Books");
        tabSheet.addTab(authorTab, "Authors");
        tabSheet.addTab(genreTab, "Genres");
        addComponents(tabSheet);
    }

}
