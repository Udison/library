package ru.udison.task.library.ui.spaces;

import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.*;
import com.vaadin.ui.components.grid.HeaderRow;
import com.vaadin.ui.renderers.ButtonRenderer;
import com.vaadin.ui.themes.ValoTheme;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import ru.udison.task.library.dao.BookDao;
import ru.udison.task.library.entity.Book;
import ru.udison.task.library.ui.spaces.addspaces.BooksAddSpace;
import ru.udison.task.library.ui.spaces.editspaces.BooksEditSpace;

import javax.annotation.PostConstruct;

@Data
@Component
@RequiredArgsConstructor
public class BooksSpace extends VerticalLayout {
    private Grid<Book> bookGrid;
    private final BookDao bookDao;
    private final BooksEditSpace booksEditSpace;
    private final BooksAddSpace booksAddSpace;

    @PostConstruct
    public void init() {
        createAddButton();
        addBooksSpace();
    }

    private void createAddButton() {
        Button addNewBook = new Button("Add new book", event ->
                booksAddSpace.drawBookAddWindow().addCloseListener(e -> updateItems()));
        addNewBook.setStyleName(ValoTheme.BUTTON_PRIMARY);
        addComponent(addNewBook);
    }

    private void addBooksSpace() {
        addComponent(createGrid());
    }

    private Grid<Book> createGrid() {
        bookGrid = new Grid<>();
        ListDataProvider<Book> provider = new ListDataProvider<>(bookDao.findAll());
        bookGrid.setDataProvider(provider);

        Grid.Column<Book, String> titleColumn = bookGrid.addColumn(Book::getTitle).setCaption("Title:");
        Grid.Column<Book, String> authorColumn = bookGrid.addColumn(book -> book.getAuthor().getFullName()).setCaption("Author:");
        Grid.Column<Book, String> publisherColumn = bookGrid.addColumn(Book::getPublisher).setCaption("Publisher:");
        bookGrid.addColumn(Book::getGenre).setCaption("Genre:");
        bookGrid.addColumn(Book::getCity).setCaption("City:");
        bookGrid.addColumn(Book::getPublishingYear).setCaption("Year:");


        HeaderRow filter = bookGrid.appendHeaderRow();

        TextField titleFilter = new TextField();
        titleFilter.addValueChangeListener(event -> provider.addFilter(
                book -> StringUtils.containsIgnoreCase(book.getTitle(), titleFilter.getValue())));
        titleFilter.setValueChangeMode(ValueChangeMode.EAGER);

        filter.getCell(titleColumn).setComponent(titleFilter);
        titleFilter.setSizeFull();
        titleFilter.setPlaceholder("Filter");

        TextField authorFilter = new TextField();
        authorFilter.addValueChangeListener(event -> provider.addFilter(
                book -> StringUtils.containsIgnoreCase(book.getAuthor().getFullName(), authorFilter.getValue())));
        authorFilter.setValueChangeMode(ValueChangeMode.EAGER);

        filter.getCell(authorColumn).setComponent(authorFilter);
        authorFilter.setSizeFull();
        authorFilter.setPlaceholder("Filter");

        TextField publisherFilter = new TextField();
        publisherFilter.addValueChangeListener(event -> provider.addFilter(
                book -> StringUtils.containsIgnoreCase(book.getPublisher(), publisherFilter.getValue())));
        publisherFilter.setValueChangeMode(ValueChangeMode.EAGER);

        filter.getCell(publisherColumn).setComponent(publisherFilter);
        publisherFilter.setSizeFull();
        publisherFilter.setPlaceholder("Filter");

        bookGrid.addColumn(author -> "Edit", new ButtonRenderer<>(clickEvent -> {

            Window editWindow = booksEditSpace.drawBookEditorWindow(clickEvent.getItem());
            editWindow.addCloseListener(e -> {
                removeComponent(bookGrid);
                addComponent(createGrid());
            });
        })).setWidth(90.0);
        bookGrid.addColumn(author -> "Remove", new ButtonRenderer<>(clickEvent -> {
            bookDao.delete(clickEvent.getItem());
            removeComponent(bookGrid);
            addComponent(createGrid());
        })).setWidth(100.0);

        bookGrid.setWidth("100%");
        return bookGrid;
    }

    public void updateItems() {
        removeComponent(bookGrid);
        addComponent(createGrid());
    }


}
