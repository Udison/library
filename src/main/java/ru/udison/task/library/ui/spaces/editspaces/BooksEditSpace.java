package ru.udison.task.library.ui.spaces.editspaces;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import ru.udison.task.library.dao.AuthorDao;
import ru.udison.task.library.dao.BookDao;
import ru.udison.task.library.dao.GenresDao;
import ru.udison.task.library.entity.Author;
import ru.udison.task.library.entity.Book;
import ru.udison.task.library.entity.Genre;

import java.util.Optional;

@Data
@SpringComponent
@RequiredArgsConstructor
public class BooksEditSpace extends Window {
    private final BookDao bookDao;
    private final GenresDao genresDao;
    private final AuthorDao authorDao;
    private Binder<Book> bookBinder;
    private Binder<Author> authorBinder;
    private Binder<Genre> genreBinder;


    public Window drawBookEditorWindow(Book book) {
        Window editWindow = new Window(String.format("Edit book: %s", book.getTitle()));
        EditSpaceConfig.configureEditWindow(editWindow, "700px", "900px");
        initBinders(book);

        VerticalLayout contentLayout = new VerticalLayout();
        contentLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);

        contentLayout.addComponents(createTitleSection(book),
                createAuthorSection(book.getAuthor()),
                createGenreSection(book.getGenre()),
                createPublisherSection(book),
                createCityAndYearSection(book),
                createSaveAndCancelButtons(book, editWindow));

        editWindow.setContent(contentLayout);

        UI.getCurrent().addWindow(editWindow);

        return editWindow;
    }

    private HorizontalLayout createSaveAndCancelButtons(Book book, Window editWindow) {

        HorizontalLayout toReturn = new HorizontalLayout();
        Button save = new Button("Save", clickEvent -> {
            try {
                authorBinder.writeBean(book.getAuthor());
                genreBinder.writeBean(book.getGenre());
                bookBinder.writeBean(book);
                authorDao.save(book.getAuthor());
                genresDao.save(book.getGenre());
                bookDao.save(book);
                Notification.show("Book was saved");
                editWindow.close();
            } catch (ValidationException e) {
                Notification.show("Book could not be saved,\n" +
                        "please check error messages for each field.", Notification.Type.ERROR_MESSAGE);
            }
        });


        save.setStyleName(ValoTheme.BUTTON_PRIMARY);

        Button cancel = new Button("Cancel", clickEvent -> editWindow.close());

        toReturn.addComponents(save, cancel);

        return toReturn;
    }

    private HorizontalLayout createCityAndYearSection(Book book) {
        HorizontalLayout toReturn = new HorizontalLayout();
        TextField cityField = new TextField("City:");
        cityField.setIcon(VaadinIcons.FACTORY);
        cityField.setStyleName(ValoTheme.TEXTFIELD_ALIGN_CENTER);
        cityField.setValue(book.getCity());
        cityField.setMaxLength(50);
        bookBinder.forField(cityField)
                .withValidator(s -> (s.matches("[a-zа-яёA-ZА-ЯЁ]+") || s.matches("")), "\"You can use letters а-Я or a-Z\"")
                .bind(Book::getCity, Book::setCity);

        TextField yearField = new TextField("Publishing year");
        yearField.setIcon(VaadinIcons.CALENDAR);
        yearField.setStyleName(ValoTheme.TEXTFIELD_ALIGN_CENTER);
        yearField.setValue(String.valueOf(book.getPublishingYear()));
        yearField.setMaxLength(4);
        bookBinder.forField(yearField)
                .withValidator(s -> (s.matches("\\d+") || s.matches("")), "Wrong data format\n" + "You can use numbers 0-9")
                .bind(Book::getPublishingYear, Book::setPublishingYear);

        toReturn.addComponentsAndExpand(cityField, yearField);

        return toReturn;

    }

    private HorizontalLayout createPublisherSection(Book book) {
        HorizontalLayout toReturn = new HorizontalLayout();
        NativeSelect<String> publisherSelect = new NativeSelect<>();
        publisherSelect.setCaption("Select a publisher");
        publisherSelect.setValue(book.getPublisher());
        publisherSelect.setRequiredIndicatorVisible(true);
        publisherSelect.setItems("Москва", "Питер", "O'Reilly");
        toReturn.addComponentsAndExpand(publisherSelect);
        bookBinder.forField(publisherSelect)
                .withValidator(s -> !s.isEmpty(), "Select a publisher")
                .bind(Book::getPublisher, Book::setPublisher);

        return toReturn;

    }

    private HorizontalLayout createGenreSection(Genre genre) {
        HorizontalLayout toReturn = new HorizontalLayout();

        TextField genreField = new TextField("Genre:");
        genreField.setIcon(VaadinIcons.PENCIL);
        genreField.setStyleName(ValoTheme.TEXTFIELD_ALIGN_CENTER);
        genreField.setValue(genre.getGenre());
        genreField.setRequiredIndicatorVisible(true);
        genreField.setMaxLength(50);
        toReturn.addComponentsAndExpand(genreField);
        genreBinder.forField(genreField)
                .withValidator(s -> s.matches("[a-zа-яёA-ZА-ЯЁ]+"), "\"You can use letters а-Я or a-Z\"")
                .bind(Genre::getGenre, Genre::setGenre);

        return toReturn;
    }

    private HorizontalLayout createAuthorSection(Author author) {
        HorizontalLayout toReturn = new HorizontalLayout();
        VerticalLayout components = new VerticalLayout();
        Label authorMark = new Label("Author", ContentMode.TEXT);
        authorMark.setStyleName(ValoTheme.TEXTFIELD_ALIGN_CENTER);
        components.addComponentsAndExpand(authorMark);

        HorizontalLayout authorSection = new HorizontalLayout();

        TextField lastName = new TextField("Last name:");
        lastName.setIcon(VaadinIcons.USER);
        lastName.setStyleName(ValoTheme.TEXTFIELD_ALIGN_CENTER);
        lastName.setValue(author.getLastName());
        lastName.setRequiredIndicatorVisible(true);
        lastName.setMaxLength(30);
        authorBinder.forField(lastName)
                .withValidator(s -> s.matches("[a-zа-яёA-ZА-ЯЁ]+"), "You can use letters а-Я or a-Z")
                .bind(Author::getLastName, Author::setLastName);

        TextField firstName = new TextField("First name:");
        firstName.setIcon(VaadinIcons.USER);
        firstName.setStyleName(ValoTheme.TEXTFIELD_ALIGN_CENTER);
        firstName.setValue(author.getFirstName());
        firstName.setRequiredIndicatorVisible(true);
        firstName.setMaxLength(30);
        authorBinder.forField(firstName)
                .withValidator(s -> s.matches("[a-zа-яёA-ZА-ЯЁ]+"), "You can use letters а-Я or a-Z")
                .bind(Author::getFirstName, Author::setFirstName);

        TextField patronymic = new TextField("Patronymic:");
        patronymic.setIcon(VaadinIcons.USER);
        patronymic.setValue(Optional
                .ofNullable(author.getPatronymic())
                .orElse(""));
        patronymic.setStyleName(ValoTheme.TEXTFIELD_ALIGN_CENTER);
        patronymic.setMaxLength(30);
        authorBinder.forField(patronymic)
                .withValidator(s -> (s.matches("[a-zа-яёA-ZА-ЯЁ]+") || s.matches("")), "You can use letters а-Я or a-Z")
                .bind(Author::getPatronymic, Author::setPatronymic);

        authorSection.addComponentsAndExpand(lastName, firstName, patronymic);
        components.addComponentsAndExpand(authorMark, authorSection);
        toReturn.addComponentsAndExpand(components);
        return toReturn;
    }

    private HorizontalLayout createTitleSection(Book book) {
        HorizontalLayout toReturn = new HorizontalLayout();
        VerticalLayout titleSection = new VerticalLayout();
        TextField title = new TextField("  Title:");
        title.setValue(book.getTitle());
        title.setWidth("800px");
        title.setIcon(VaadinIcons.BOOK);
        title.setStyleName(ValoTheme.TEXTFIELD_ALIGN_CENTER);
        title.setStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);
        title.setRequiredIndicatorVisible(true);
        title.setMaxLength(80);
        bookBinder.forField(title)
                .withValidator(s -> s.matches("[a-zа-яёA-ZА-ЯЁ\\s]+"), "You can use letters а-Я or a-Z")
                .bind(Book::getTitle, Book::setTitle);
        Label counter = new Label();
        counter.setValue(title.getValue().length() +
                " of " + title.getMaxLength());

        // Display the current length interactively in the counter
        title.addValueChangeListener(event -> {
            int len = event.getValue().length();
            counter.setValue(len + " of " + title.getMaxLength());
        });
        title.setValueChangeMode(ValueChangeMode.EAGER);
        titleSection.setWidth("900px");
        titleSection.addComponents(title, counter);
        toReturn.addComponent(titleSection);

        return toReturn;
    }

    private void initBinders(Book book) {
        bookBinder = new Binder<>();
        bookBinder.readBean(book);
        authorBinder = new Binder<>();
        authorBinder.readBean(book.getAuthor());
        genreBinder = new Binder<>();
        genreBinder.readBean(book.getGenre());
    }

}
