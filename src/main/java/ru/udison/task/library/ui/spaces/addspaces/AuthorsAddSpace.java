package ru.udison.task.library.ui.spaces.addspaces;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.udison.task.library.dao.AuthorDao;
import ru.udison.task.library.entity.Author;

@Data
@Component
@RequiredArgsConstructor
public class AuthorsAddSpace extends Window {
    private final AuthorDao authorDao;
    private Binder<Author> authorBinder;

    public Window drawAuthorAddWindow() {
        Window addWindow = new Window("Add new author");
        AddSpaceConfig.configureAddWindow(addWindow, "350px", "300px");

        VerticalLayout contentLayout = new VerticalLayout();

        Author author = new Author();

        initBinder(author);

        contentLayout.addComponentsAndExpand(createFields());
        contentLayout.addComponent(createSaveAndCancelButtons(author, addWindow));

        addWindow.setContent(contentLayout);
        UI.getCurrent().addWindow(addWindow);

        return addWindow;
    }

    private HorizontalLayout createSaveAndCancelButtons(Author author, Window addWindow) {

        HorizontalLayout toReturn = new HorizontalLayout();
        Button save = new Button("Save", event -> {
            try {
                authorBinder.writeBean(author);
                if(authorDao.existsAuthorByLastNameAndFirstNameAndPatronymic(author.getLastName(), author.getFirstName(), author.getPatronymic())) {
                    Notification.show("Author is exists yet", Notification.Type.WARNING_MESSAGE);
                    return;
                }
                authorDao.save(author);
                Notification.show("Author was saved");
                addWindow.close();
            } catch (ValidationException e) {
                Notification.show("Author could not be saved,\n" +
                        "please check error messages for each field.", Notification.Type.ERROR_MESSAGE);
            }
        });
        save.setStyleName(ValoTheme.BUTTON_PRIMARY);

        Button cancel = new Button("Cancel", event -> addWindow.close());

        toReturn.addComponentsAndExpand(save, cancel);

        return  toReturn;
    }

    private void initBinder(Author author) {
        authorBinder = new Binder<>();
        authorBinder.readBean(author);
    }

    private HorizontalLayout createFields() {
        HorizontalLayout toReturn = new HorizontalLayout();
        VerticalLayout components = new VerticalLayout();

        TextField lastName = new TextField("Last name:");
        configureTextField(lastName, true, 50);
        authorBinder.forField(lastName)
                .withValidator(s -> s.matches("[a-zа-яёA-ZА-ЯЁ]+"), "You can use letters а-Я or a-Z")
                .bind(Author::getLastName, Author::setLastName);

        TextField firstName = new TextField("First name:");
        configureTextField(firstName, true, 30);
        authorBinder.forField(firstName)
                .withValidator(s -> s.matches("[a-zа-яёA-ZА-ЯЁ]+"), "You can use letters а-Я or a-Z")
                .bind(Author::getFirstName, Author::setFirstName);

        TextField patronymic = new TextField("Patronymic:");

        configureTextField(patronymic, false, 30);
        authorBinder.forField(patronymic)
                .withValidator(s -> (s.matches("[a-zа-яёA-ZА-ЯЁ]+") || s.matches("")), "You can use letters а-Я or a-Z")
                .bind(Author::getPatronymic, Author::setPatronymic);

        components.addComponentsAndExpand(lastName, firstName, patronymic);
        toReturn.addComponentsAndExpand(components);
        return toReturn;

    }

    private void configureTextField(TextField field, boolean required, int maxLength) {
        field.setSizeFull();
        field.setMaxLength(maxLength);
        field.setIcon(VaadinIcons.USER);
        field.setRequiredIndicatorVisible(required);
        field.setStyleName(ValoTheme.TEXTFIELD_ALIGN_CENTER);
    }

}
