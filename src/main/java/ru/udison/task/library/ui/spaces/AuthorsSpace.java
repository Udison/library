package ru.udison.task.library.ui.spaces;

import com.vaadin.ui.*;
import com.vaadin.ui.renderers.ButtonRenderer;
import com.vaadin.ui.themes.ValoTheme;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.udison.task.library.dao.AuthorDao;
import ru.udison.task.library.dao.BookDao;
import ru.udison.task.library.entity.Author;
import ru.udison.task.library.ui.spaces.addspaces.AuthorsAddSpace;
import ru.udison.task.library.ui.spaces.editspaces.AuthorsEditSpace;

import javax.annotation.PostConstruct;

@Data
@Component
@RequiredArgsConstructor
public class AuthorsSpace extends VerticalLayout {
    private Grid<Author> authorGrid;
    private final AuthorDao authorDao;
    private final BookDao bookDao;
    private final AuthorsEditSpace authorsEditSpace;
    private final AuthorsAddSpace authorsAddSpace;

    @PostConstruct
    public void init() {
        createAddButton();
        createAuthorsSpace();
    }

    private void createAuthorsSpace() {
        authorGrid = new Grid<>(Author.class);
        authorGrid.setItems(authorDao.findAll());
        authorGrid.removeColumn("authorId");
        authorGrid.setColumnOrder("lastName", "firstName", "patronymic");

        authorGrid.addColumn(author -> "Edit", new ButtonRenderer<>(clickEvent -> {

            Window editWindow = authorsEditSpace.drawAuthorEditWindow(clickEvent.getItem());
            editWindow.addCloseListener(e -> authorGrid.setItems(authorDao.findAll()));
        })).setWidth(90.0);
        authorGrid.addColumn(author -> "Remove", new ButtonRenderer<>(clickEvent -> {
            if(bookDao.findAllByAuthor(clickEvent.getItem()).isEmpty()) {
                authorDao.delete(clickEvent.getItem());
                authorGrid.setItems(authorDao.findAll());
            }
            else {
                Notification.show("Can't remove this author. The library has book written by him.", Notification.Type.WARNING_MESSAGE);
            }
        })).setWidth(100.0);
        authorGrid.setWidth("100%");
        addComponentsAndExpand(authorGrid);
    }

    private void createAddButton() {
        Button addNewAuthor = new Button("Add new author", event -> {
            Window addWindow = authorsAddSpace.drawAuthorAddWindow();
            addWindow.addCloseListener(e -> authorGrid.setItems(authorDao.findAll()));
        });
        addNewAuthor.setStyleName(ValoTheme.BUTTON_PRIMARY);
        addComponent(addNewAuthor);
    }

    void updateItems() {
        authorGrid.setItems(authorDao.findAll());
    }

}
