package ru.udison.task.library.ui.spaces.editspaces;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.udison.task.library.dao.AuthorDao;
import ru.udison.task.library.entity.Author;

import java.util.Optional;

@Data
@Component
@RequiredArgsConstructor
public class AuthorsEditSpace extends Window {
    private final AuthorDao authorDao;
    private Binder<Author> authorBinder;

    public Window drawAuthorEditWindow(Author author) {
        Window editWindow = new Window(String.format("Edit author: %s", author.getFullName()));
        EditSpaceConfig.configureEditWindow(editWindow, "350px", "300px");

        VerticalLayout contentLayout = new VerticalLayout();

        initBinder(author);

        contentLayout.addComponentsAndExpand(createFields(author));
        contentLayout.addComponent(createSaveAndCancelButtons(author, editWindow));

        editWindow.setContent(contentLayout);
        UI.getCurrent().addWindow(editWindow);

        return editWindow;
    }

    private HorizontalLayout createSaveAndCancelButtons(Author author, Window editWindow) {

        HorizontalLayout toReturn = new HorizontalLayout();
        Button save = new Button("Save", event -> {
            try {
                authorBinder.writeBean(author);
                if(authorDao.existsAuthorByLastNameAndFirstNameAndPatronymic(author.getLastName(), author.getFirstName(), author.getPatronymic())) {
                    Notification.show("Author is exists yet", Notification.Type.WARNING_MESSAGE);
                    return;
                }
                authorDao.save(author);
                Notification.show("Author was saved");
                editWindow.close();
            } catch (ValidationException e) {
                Notification.show("Author could not be saved,\n" +
                        "please check error messages for each field.", Notification.Type.ERROR_MESSAGE);
            }
        });
        save.setStyleName(ValoTheme.BUTTON_PRIMARY);

        Button cancel = new Button("Cancel", event -> editWindow.close());

        toReturn.addComponentsAndExpand(save, cancel);

        return  toReturn;
    }

    private void initBinder(Author author) {
        authorBinder = new Binder<>();
        authorBinder.readBean(author);
    }

    private HorizontalLayout createFields(Author author) {
        HorizontalLayout toReturn = new HorizontalLayout();
        VerticalLayout components = new VerticalLayout();

        TextField lastName = new TextField("Last name:");
        lastName.setValue(author.getLastName());
        configureTextField(lastName, true, 50);
        authorBinder.forField(lastName)
                .withValidator(s -> s.matches("[a-zа-яёA-ZА-ЯЁ]+"), "You can use letters а-Я or a-Z")
                .bind(Author::getLastName, Author::setLastName);

        TextField firstName = new TextField("First name:");
        firstName.setValue(author.getFirstName());
        configureTextField(firstName, true, 30);
        authorBinder.forField(firstName)
                .withValidator(s -> s.matches("[a-zа-яёA-ZА-ЯЁ]+"), "You can use letters а-Я or a-Z")
                .bind(Author::getFirstName, Author::setFirstName);

        TextField patronymic = new TextField("Patronymic:");
        patronymic.setValue(Optional
                .ofNullable(author.getPatronymic())
                .orElse(""));
        configureTextField(patronymic, false, 30);
        authorBinder.forField(patronymic)
                .withValidator(s -> (s.matches("[a-zа-яёA-ZА-ЯЁ]+") || s.matches("")), "You can use letters а-Я or a-Z")
                .bind(Author::getPatronymic, Author::setPatronymic);

        components.addComponentsAndExpand(lastName, firstName, patronymic);
        toReturn.addComponentsAndExpand(components);
        return toReturn;

    }

    private void configureTextField(TextField field, boolean required, int maxLength) {
        field.setSizeFull();
        field.setMaxLength(maxLength);
        field.setIcon(VaadinIcons.USER);
        field.setRequiredIndicatorVisible(required);
        field.setStyleName(ValoTheme.TEXTFIELD_ALIGN_CENTER);
    }

}
