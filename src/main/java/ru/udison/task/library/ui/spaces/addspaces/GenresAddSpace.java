package ru.udison.task.library.ui.spaces.addspaces;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.udison.task.library.dao.GenresDao;
import ru.udison.task.library.entity.Genre;

@Data
@Component
@RequiredArgsConstructor
public class GenresAddSpace extends Window {
    private final GenresDao genresDao;
    private Binder<Genre> genreBinder;


    public Window drawGenreAddWindow() {
        Window editWindow = new Window("Add new genre");
        AddSpaceConfig.configureAddWindow(editWindow, "180px", "300px");

        VerticalLayout contentLayout = new VerticalLayout();
        contentLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);

        Genre genre = new Genre();

        initBinder(genre);

        contentLayout.addComponentsAndExpand(createGenreSection());
        contentLayout.addComponent(createSaveAndCancel(genre, editWindow));

        editWindow.setContent(contentLayout);
        UI.getCurrent().addWindow(editWindow);

        return editWindow;
    }

    private HorizontalLayout createSaveAndCancel(Genre genre, Window editWindow) {
        HorizontalLayout buttonsSection = new HorizontalLayout();

        Button save = new Button("Save", event -> {
            try {
                genreBinder.writeBean(genre);
                if(genresDao.existsByGenre(genre.getGenre())) {
                    Notification.show("Genre is exists yet", Notification.Type.WARNING_MESSAGE);
                    return;
                }
                genresDao.save(genre);
                Notification.show("Genre was saves");
                editWindow.close();
            } catch (ValidationException e) {
                Notification.show("Genre could not be saved,\n" +
                        "please check error messages for each field.", Notification.Type.ERROR_MESSAGE);
            }
        });
        save.setStyleName(ValoTheme.BUTTON_PRIMARY);

        Button cancel = new Button("Cancel", event -> editWindow.close());

        buttonsSection.addComponentsAndExpand(save, cancel);
        return buttonsSection;
    }

    private void initBinder(Genre genre) {
        genreBinder = new Binder<>();
        genreBinder.readBean(genre);
    }

    private HorizontalLayout createGenreSection() {
        HorizontalLayout toReturn = new HorizontalLayout();
        TextField genreField = new TextField("Genre:");
        configureField(genreField);
        genreBinder.forField(genreField)
                .withValidator(s -> s.matches("[a-zа-яёA-ZА-ЯЁ\\s]+"), "\"You can use letters а-Я or a-Z\"")
                .bind(Genre::getGenre, Genre::setGenre);
        toReturn.addComponentsAndExpand(genreField);

        return toReturn;
    }

    private void configureField(TextField genreField) {
        genreField.setSizeFull();
        genreField.setIcon(VaadinIcons.PENCIL);
        genreField.setRequiredIndicatorVisible(true);
        genreField.setMaxLength(50);
        genreField.setStyleName(ValoTheme.TEXTFIELD_ALIGN_CENTER);
    }
}
